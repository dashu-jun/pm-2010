export default[ //需要鉴权的页面
    {
        path:"*",
        component: ()=>import(/*webpackChunkName:'page404'*/"../pages/Page404"),
        name:"page404"
    },
    {
        path:"/",
        redirect: "/welcome",
        component: ()=>import(/*webpackChunkName:'Home'*/"../pages/Home"),
        name:"home",
    },
]