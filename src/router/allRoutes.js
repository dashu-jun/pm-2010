const allRoutes = [
    {
        path:"welcome",
        component:()=>import(/*webpackChunkNmae:"welcome"*/"../pages/Home/Welcome"),
        name:"welcome",
        meta:{
            //存储一些属性
            fullPath:"/Welcome",
            icon:"iconfont icon-fuwulei",
            name:"管理首页"
        }
    },
    {
        path:"attendance",
        component:()=>import(/*webpackChunkNmae:"attendance"*/"../pages/Home/Attendance"),
        name:"attendance",
        meta:{
            //存储一些属性
            fullPath:"/Attendance",
            icon:"iconfont icon-quanbuyingyong",
            name:"考勤管理"
        }
    },    {
        path:"StudentManager",
        component:()=>import(/*webpackChunkNmae:"StudentManager"*/"../pages/Home/StudentManager"),
        name:"StudentManager",
        redirect: "/StudentManager/product",
        meta:{
            //存储一些属性
            fullPath:"/StudentManager",
            icon:"iconfont icon-weiyingyong",
            name:"学员管理"
        },
        children:[
            {
                path:"dormitory",
                component:()=>import(/*webpackChunkNmae:"dormitory"*/"../pages/Home/StudentManager/StudentDormitory"),
                name:"dormitory",
                meta:{
                    //存储一些属性
                    fullPath:"/studentManager/dormitory",
                    icon:"iconfont icon-yingyonglei",
                    name:"学员宿舍"
                }
            },
            {
                path:"product",
                component:()=>import(/*webpackChunkNmae:"product"*/"../pages/Home/StudentManager/StudentProduct"),
                name:"product",
                meta:{
                    //存储一些属性
                    fullPath:"/studentManager/product",
                    icon:"iconfont icon-caiwulei",
                    name:"学员项目管理"
                }
            },
            {
                path:"profill",
                component:()=>import(/*webpackChunkNmae:"profill"*/"../pages/Home/StudentManager/StudentProfill"),
                name:"profill",
                meta:{
                    //存储一些属性
                    fullPath:"/studentManager/profill",
                    icon:"iconfont icon-b17",
                    name:"学员资料"
                }
            },
        ]
    },
    {
        path:"users",
        component:()=>import(/*webpackChunkNmae:"users"*/"../pages/Home/Users"),
        name:"users",
        meta:{
            //存储一些属性
            fullPath:"/Users",
            icon:"iconfont icon-user",
            name:"用户管理"
        }
    },
    {
        path:"mine",
        component:()=>import(/*webpackChunkNmae:"mine"*/"../pages/Home/Mine"),
        name:"mine",
        meta:{
            //存储一些属性
            fullPath:"/Mine",
            icon:"iconfont icon-weitianerweima",
            name:"我的中心"
        }
    },
    {
        path:"statistics",
        component:()=>import(/*webpackChunkNmae:"statistics"*/"../pages/Home/Statistics"),
        name:"statistics",
        meta:{
            //存储一些属性
            fullPath:"/Statistics",
            icon:"iconfont icon-diannaopcxianshiqi",
            name:"数据统计"
        }
    },
]

export default allRoutes