import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import ElementUI from 'element-ui';//核心js
import 'element-ui/lib/theme-chalk/index.css';//css文件
import store from "./store";
import "./assets/styles/common.css";
import "./assets/styles/iconfont.css";//字体图标
import qfSubMenu from "qf-sub-menu";
import bus from "@/utils/eventBus.js"
Vue.prototype.$bus = bus;
Vue.use(ElementUI);
Vue.use(qfSubMenu);
// Vue.config.productionTip = false;


//路由守卫，核心：路由的全局前置钩子brforeEach

router.beforeEach(function (to, from, next) {
  //判断本次存储中是否有token，有就next()，没有就跳转到登陆页面
  let token = localStorage.getItem("pm-token")
  //判断用户访问的是否是login页面，如果是则直接放行，不是则验证是否携带token，有就放行，没有则跳转到login页面
  if (to.path === "/login") {
    next()
  } else {
    if (token) {
      //判断vuex中store里面的sideMenu数组长度是否为0.如果为0，那么重新调用action中获取用户菜单的方法，计算用户菜单并且渲染
      let { sideMenu } = store.state;
      if (sideMenu && sideMenu.length === 0) {
        //调用action中获取菜单的方法
        store.dispatch('FETCH_MENULIST')
          .then(() => {
            next({ path: to.path })//这里注意，next里面的参数是要进入页面的路由信息。因为当next传参数后，就会将原本next不传参时要进入的路由废止，转而进入参数对应的路由，虽然是同一个路由，但这么做主要是为了确保addRoutes生效。
          })
      } else {
        next()
      }

    } else {
      next({ path: "/login" })
    }
  }
});

//全局的后置钩子
router.afterEach((to, from) => {
  // console.log(to);
  //干掉第0项
  let breadCrum = [...to.matched]
  breadCrum.splice(0, 1)
  store.commit('SET_BREADCRUM', breadCrum)
})


new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
