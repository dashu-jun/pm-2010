import Vue from "vue";
import Vuex from "vuex";
import {getMenuList} from "@/api"
import recursionRoutes from "@/utils/recursionRoutes"
import allRoutes from "@/router/allRoutes"

import dynamicRoutes from "../router/dynamicRoutes"
import router from "../router"
Vue.use(Vuex);

//1、发送请求，获取用户菜单名
//2、通过递归函数计算当前用户的路由配置
//3、将计算好的路由配置添加到路由"/"的children


export default new Vuex.Store({
  state: {//存储状态，那些被其他组件共享的数据都可以称为state
    //vuex中的state和视图是一一对应的
    sideMenu:[],
    breadCrum: [],
  },
  mutations: {
    //更改state的唯一方式
    //一个type对应一个handle
    //mutation中逻辑必须是同步的
    SET_SIDEMENU(state,payload){
      state.sideMenu = payload;
      // console.log(state.sideMenu);
      //查到到dynamicRoutes中路径为"/"的对象，并且给它添加一个children属性，值就是计算好的sideMenu
      let target = dynamicRoutes.find(item=>{
        return item.path==="/"
      })
      target.children = state.sideMenu;
      //核心方法：通过addRoutes这个方法，动态的将路由配置添加到项目中去
      router.addRoutes(dynamicRoutes)
    },
    //清空用户菜单
    CLEAR_SIDMENU(state){
      state.sideMenu = []
    },
    //更改面包屑
    SET_BREADCRUM(state,payload){
      state.breadCrum = payload
    }
  },
  actions: {
    //action提交的是mutation，而不是直接变更状态，
    //action可以包含任意异步操作
    //一个类型对应一个函数，接收的参数是context
    async FETCH_MENULIST({commit}){
      //发送请求获取用户菜单名
      let res=await getMenuList()
      //调用recursionRoutes计算出用户的路由配置
      // console.log(res);
      if(res.data.state&&res.data.menuList.length>0){
        let userMenu =  recursionRoutes(res.data.menuList,allRoutes)
        commit("SET_SIDEMENU",userMenu)
      }else{
        //获取路由失败
        
      }
      //再提交给mutation

    }
  },
  modules: {},
  getters: {},
});
