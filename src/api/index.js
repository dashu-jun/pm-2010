import axios from 'axios'
import service from './config'

//登入
/**
 * 
 * @param {String} username 用户名
 * @param {String} password 密码
 * @returns voied
 */
export const login = (username, password) => {
  return service.post("/api/users/login", {
    username,
    password
  })
}
//获取验证码

export const getCaptcha = () => service.get("/api/users/getCaptcha")


//校验验证码 

export const verifyCaptcha = captcha => service.get(`/api/users/verifyCaptcha?captcha=${captcha}`)

//获取登陆日志
export const getlog = (page = 1, count = 20) => {
  return service.get(`/api/getloginlog?page=${page}&count=${count}`)
}

//获取权限菜单
export const getMenuList = () => {
  return service.get("/api/permission/getMenuList")
}

//获取列表
export const getStuList = (params = { page: 1, count: 10, classes: "" }) => service.get("/api/students/getstulist", {
  params: {
    page: params.page,
    count: params.count,
    class: params.classes
  }
})
//获取学员名字搜索学员
export const searchStu = (page = 1, count = "", key = "") => service.get("/api/students/searchstu", {
  params: {
    page,
    count,
    key
  }
})

//获取任务数据
export const getTarskData = () => service.get("/data/tarsk.json")
//获取rate数据
export const getRateData = () => service.get("/data/rate.json")
//获取expect数据
export const getExpectData = () => service.get("/data/expect.json")

//增加学员信息

export const addStuReq = (params = {}) => service.post("/api/students/addstu", params)

//编辑学员信息

export const editStuReq = (params = {}) => service.post("/api/students/updatestu", params)

//删除学员信息

export const deleteStu = (sId) => service.get("/api/students/delstu", {
  params: {
    sId
  }
})
