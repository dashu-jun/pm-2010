import axios from 'axios'
import router from "@/router"
import ElementUI from "element-ui"
const service = axios.create({
    // baseURL: "/api",
    withCredentials: true,//允许客户端携带认证给服务器,将cookie传递给服务器
    timeout: 1000 * 10//请求不超过10秒
})

//请求拦截器
service.interceptors.request.use(function(config){
    //登陆请求，验证码请求不需要携带token
    //当有多个路径不需要携带token的时候，可以声明一个数组来接收它们
    let accessPath=["/users/getCaptcha","/users/login"]
    //用some()方法来判断用户访问的地址是否有需要携带token
    let r = accessPath.some(item=>config.url===item)
    if(r){//不需要携带token
        return config
    }else{
        let token = localStorage.getItem("pm-token");
        config.headers['authorization']=token;
        return config
    }
})
//未登录的状态码是10022，验证码错误的状态是1004

//响应拦截器
service.interceptors.response.use(function(config){
    //如果是响应验证码就不需要拦截
    let regExp = /^\/users\/verifyCaptcha.+/
    let {url} = config.config
    // console.log(url);
    if(url==="/users/getCaptcha" || regExp.test(url) || url === "/users/login"){
        return config
    }else{//其他请求响应
        //如果响应的状态是10022或者是1004，那么需要跳转到登陆页面
        if(config.data.code==="10022" || config.data.code==="1004"){
            ElementUI.Message.error("登陆过期，请重新登陆");
            router.push("/login");
            localStorage.removeItem('pm-token')
        }else{
            return config
        }

    }
    
})

export default service