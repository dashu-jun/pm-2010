//用于根据获取下来的数据和allRoutes进行对比，生成用户菜单和页面

/**
 * 
 * @param {Array} userMenu 从远程服务器获取下来的用户菜单的名字
 * @param {Array} allRoutes 整个项目的路由配置文件
 */


export default function recursionRoutes(userMenu,allRoutes){
    // console.log(userMenu);
    // console.log(allRoutes);
    let menuList = []
    userMenu.forEach(item=>{
        allRoutes.forEach(v=>{
            if(item.name===v.meta.name){
                if(item.children&&item.children.length>0){
                   v.children = recursionRoutes(item.children,v.children)
                }
                menuList.push(v)
            }
        })
    })
    console.log(menuList);
    return menuList
}