const path = require('path');
function resolve(dir) {
  return path.join(__dirname, dir)
}

let baseUrl = process.env.VUE_APP_BASE_URL
let dataUrl = process.env.VUE_APP_DATA_URL
// console.log(process.env.NODE_ENV);//获取当前开发环境
// console.log(baseUrl);
module.exports = {
  lintOnSave: false,
  devServer: {
    port: 8080,
    proxy: {//代理
      "/api": {//代理地址
        target: baseUrl,//目标服务器
        pathRewrite: {//路径重写
          "^/api": ""
        }
      },
      "/data": {//代理地址
        target: dataUrl,//目标服务器
      },
    }
  },
  chainWebpack: (config) => {
    config.resolve.alias //配置别名
      .set('$', resolve('src'))
      .set('assets', resolve('src/assets'))
      .set('components', resolve('src/components'))
      .set('layout', resolve('src/layout'))
      .set('base', resolve('src/base'))
      .set('static', resolve('src/static'))
  }
};
// http://chst.vip:8081/api/data/index.json 代理后
// http://chst.vip:8081/data/index.json 真实地址
